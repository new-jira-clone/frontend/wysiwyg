FROM node:18-alpine AS build-stage
WORKDIR /wysiwyg
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build
FROM nginx:alpine as production-stage
COPY --from=build-stage /wysiwyg/dist /usr/share/nginx/html
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf 
EXPOSE 82
CMD ["nginx", "-g", "daemon off;"]
