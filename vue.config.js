const path = require('path');
const { defineConfig } = require('@vue/cli-service');
const { ModuleFederationPlugin } = require('webpack').container;
const dev = require('./package.json');

module.exports = defineConfig({
  publicPath: process.env.VUE_APP_WYSIWYG,
  css: {
    loaderOptions: {
      sass: {
        additionalData: `
          @use "./node_modules/@new-jira-clone/ui/src/styles/tools/index.scss" as tools;
          `,
      },
    },
  },
  transpileDependencies: true,
  configureWebpack: {
    resolve: {
      alias: {
        '@tools': path.resolve(
          __dirname,
          './node_modules/@new-jira-clone/ui/src/styles/tools/index.scss'
        ),
        '@uiIconMixins': path.resolve(
          __dirname,
          './node_modules/@new-jira-clone/ui/src/components/icon/JIcon/mixins.scss'
        ),
      },
    },
    optimization: {
      splitChunks: false,
    },
    entry: path.resolve(__dirname, './src/index.ts'),
    plugins: [
      new ModuleFederationPlugin({
        name: 'wysiwyg',
        filename: 'remoteEntry.js',
        exposes: {
          './WEditor': './src/components/WEditor',
        },
        shared: {
          ...dev.dependencies,
          vue: {
            singleton: true,
            requiredVersion: dev.dependencies.vue,
          },
        },
      }),
    ],
    devServer: {
      port: 3002,
      allowedHosts: 'auto',
      hot: true,
    },
  },
});
